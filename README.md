# Project Nigel

The purpose of SBIR Phase I proposal is to demonstrate the feasibility of developing software that significantly improves automated data acquisition speed by making better use of hardware, and by allowing imaging of multiplexed sample grids.

* Improve data acquisition efficiency
* Automate imaging of multiplexed grids and create a web-based interface
* Testing and validation

This repository contains code used in part to develop a prototype for Project Nigel. The code was developed by Craig Yoshioka, Lead R&D Developer/Scientist of NanoImaing Services with the assistance of Rise Riyo, R&D Developer.

# Description: _zgmp_

_zgmp_ is a Python library that overcomes some deficiences in the Python standard library's _multiprocessing_ module and its use of the _gevent_ library.

* _Inflexible serialization of functions_: By using the _dill library, we serialize a wider variety of Python objects than the built-in _pickling_ module.
* _Platform specific oddities_: Forking vs. spawning and presence of native OS pipe objects for communication between processes bring up a number of issues. By using the _zmq_ messaging sockets and the _multiprocessing_ module, we resolve these issues across platforms.

We use a cache to store messages (i.e., commands and data) as hashed strings in a dictionary. In doing so, we increase the speed so that Python objects need only be sent once to a worker handle.

The _pyzmq_ driver includes built-in support for _gevent_ from which we inherit the asynchronous behavior in our implementation process pool.

We use the _multiprocessing_ module to launch a separate Python instance to handle each message with an initialized IO eventloop and signal handler.

# Dependencies

_zgmp_ contains as dependencies: _pyzmq_, _gevent_, and _dill_.

* _pyzmq_ sets up the socket-layers
* _gevent_ handles the _greenlets_ which are simply co-routines
* _dill_ serializes/deserializes the messages

Note: See Installation

# Documentation

Not available yet.

# Installation

One can install _zgmp_ in the following way:

If [Anaconda](www.continuum.io) or [Miniconda](www.continuum.io) is already installed, use `conda` to install _zgmp_.

        conda install --channel https://conda.binstar.org/nigel zgmp

Note: Installation of _zgmp_ via `conda` will install the dependencies of _zgmp_ listed above as well.

# Usage

Using zgmp should look like this:

	#!python

	import zgmp

	pool = zgmp.Pool(10)
	print pool(lambda: 'hello world').get()

