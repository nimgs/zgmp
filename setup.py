
import os
from setuptools import setup

# Utility function to read the README file.
def read(fname):
	return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
	name = "zgmp",
	version = "0.0.9",
	packages = ['zgmp'],

	description = "Gevent, Multiprocessing for NIS Pyscope-2",
	author = "NanoImagingServices",
	author_email = "zoidberg@example.com",
	url = "http://www.nanoimagingservices.com/",
	long_description = read('README.txt'),

	exclude_package_data = { ' ': ['*.pyc', '.DS_Store']},
	install_requires = [
		'dill',
		'gevent',
		'pyzmq',
	]

)
