
import unittest
import zgmp.pool

class TestPool(unittest.TestCase):

	def method(self, *args, **kwargs):
		return args, kwargs

	def test_pool_lambda(self):
		pool = zgmp.pool.Pool(10)
		assert pool.a(lambda: 'hello').get() == 'hello'
		
	def test_pool_async_method(self):
		pool = zgmp.pool.Pool(10)
		assert pool.a(self.method, 1, 2, 3).get() == ((1,2,3), {})

	def test_pool_sync_method(self):
		pool = zgmp.pool.Pool(10)
		assert pool.s(self.method, 1, 2, 3) == ((1,2,3), {})

	def test_pool_proxied_method(self):
		pool = zgmp.pool.Pool(10)
		proxied = pool.proxy(self.method)
		assert proxied(1,2,3) == ((1,2,3), {})

	def test_pool_is_parallel(self):
		import time
		pool = zgmp.pool.Pool(10)
		start = time.time()
		promises = [pool.a(time.sleep, 0.01) for _ in range(10)]
		results  = [promise.get() for promise in promises]
		elapsed  = time.time() - start
		assert elapsed < 0.02
