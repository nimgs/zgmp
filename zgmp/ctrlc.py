
'''
importing this module ensures that 
invoking ctrl-c really kills the python 
process, despite what is going on in threads and subprocesses
'''

import signal

signal.signal(signal.SIGINT, signal.SIG_DFL)

