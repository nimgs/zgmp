
import ctrlc
import gevent
from gevent.queue import Queue, Empty
from contextlib import contextmanager

from worker import WorkerHandle

class Pool(object):

	def __init__(self, workers=4):
		self.max_workers = workers
		self.idle_workers = Queue()
		self.live_workers = []

		for _ in range(workers):
			self.add_worker()


	def add_worker(self):
		worker = WorkerHandle()
		self.live_workers += [worker]
		self.idle_workers.put(worker)

	def submit(self, func, *args, **kwargs):
		with self.idleworker as worker:
			return worker(func, *args, **kwargs)

	def s(self, func, *args, **kwargs):
		return self.submit(func, *args, **kwargs)

	def a(self, func, *args, **kwargs):
		return gevent.spawn(self.submit, func, *args, **kwargs)

	def amap(self, func, items):
		return [self.a(func, item) for item in items]

	def map(self, func, items):
		ps = [self.a(func, item) for item in items]
		return [p.get() for p in ps]

	@property
	@contextmanager
	def idleworker(self):
		'''
		yield an idle worker then replace it when done
		'''
		worker = self.idle_workers.get()
		yield worker
		self.idle_workers.put(worker)

	def proxy(self, func):
		def proxied(*args, **kwargs):
			return self.s(func, *args, **kwargs)
		return proxied

def pooled(func, workers=10):
	pool = Pool(workers)
	def proxy(*args, **kwargs):
		return pool(func, *args, **kwargs)
	proxy.func_name = "pooled_%s"%(get_callable_name(func))
	return proxy

def get_callable_name(obj):
	if hasattr(obj, 'func_name'):
		return obj.func_name
	elif hasattr(obj, '__class__'):
		return obj.__class__.__name__
	return obj.__name__


