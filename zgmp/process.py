

import multiprocessing as mp


def wrapper(func, args, kwargs):
	import reset
	reset.all()
	return func(*args, **kwargs)

class Process(mp.Process):

	def __init__(self, func, *args, **kwargs):
		mp.Process.__init__(self, target=wrapper, args=[func, args, kwargs])

def spawn(func, *args, **kwargs):
	process = Process(func, *args, **kwargs)
	process.start()
	return process

def daemon(func, *args, **kwargs):
	process = Process(func, *args, **kwargs)
	process.daemon = True
	process.start()
	return process

