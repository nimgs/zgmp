
from pool import Pool
from worker import WorkerHandle

def proxy(obj):
	def remote(methodname, *args, **kwargs):
		return getattr(obj, methodname)(*args, **kwargs)
	return remote


class Proxy(object):

	def __init__(self, obj):
		self.worker = WorkerHandle()
		self.caller = proxy(obj)
		self.obj = obj

	def __getattr__(self, methodname):
		attr = getattr(self.obj, methodname, None)
		if not callable(attr):
			raise RuntimeError('only callable attributes can be proxied')
		def invoke(*args, **kwargs):
			return self.worker(self.caller, methodname, *args, **kwargs)
		return invoke


class ProxyPool(object):

	def __init__(self, obj, count=10):
		self.pool = Pool(count)
		self.caller = proxy(obj)
		self.obj = obj

	def __getattr__(self, methodname):
		attr = getattr(self.obj, methodname, None)
		if not callable(attr):
			raise RuntimeError('only callable attributes can be proxied')
		def invoke(*args, **kwargs):
			return self.pool(self.caller, methodname, *args, **kwargs).get()
		return invoke
