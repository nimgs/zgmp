

def reset_signal():
	'''
	reset signal handlers in forked python process
	cribbed from gipc package
	'''
	import signal
	allsigs = [s for s in dir(signal) if s.startswith("SIG")]
	ignores = ['SIG_DFL', 'SIGSTOP', 'SIGKILL', 'SIGPIPE', 'SIG_IGN']
	def reset_to_default(signame):
		if hasattr(signal, signame):
			signum = getattr(signal, signame)
			if signum < signal.NSIG:
				signal.signal(signum, signal.SIG_DFL)
	map(reset_to_default, set(allsigs)-set(ignores))

def reset_gevent():
	'''
	reset copy of gevent ioloop after forking
	cribbed from gipc package
	'''
	try:
		import gevent
		gevent.reinit()
		hub = gevent.get_hub()
		del hub.threadpool
		hub._threadpool = None
		hub.destroy(destroy_loop=True)
		hub = gevent.get_hub(default=True)
	except:
		pass

def all():
	'''
	things that need resetting after forking, not needed on windows, but also doesn't hurt
	'''
	reset_signal()
	reset_gevent()

