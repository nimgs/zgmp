

import dill
import zmq.green as zmq

class Worker(object):

	def __init__(self, address, port):
		self.func_cache = {}
		self.init(address, port)
		self.start()

	def init(self, address, port):
		import dill
		import zmq.green as zmq
		context = zmq.Context()
		socket = context.socket(zmq.PAIR)
		socket.connect("%s:%d"%(address, port))
		def send(data):
			socket.send(dill.dumps(data))
		def recv():
			return dill.loads(socket.recv())
		self.send = send
		self.recv = recv
		
	def cache_func(self, funcid, func):
		if funcid in self.func_cache:
			return self.func_cache[funcid]
		self.func_cache[funcid] = func
		return func

	def start(self):
		while True:
			
			funcid, func, args, kwargs = self.recv()
			
			if funcid is None:
				# exit loop
				return

			result = {}
			
			try:
				func = self.cache_func(funcid, func)
				result['ok'] = func(*args, **kwargs)
			except Exception as error:
				import traceback
				result['error'] = {
					'exception': error,
					'traceback': traceback.format_exc(),
				}

			self.send(result)

class WorkerHandle(object):

	socket = None

	def __init__(self, *args, **kwargs):
		self.funcs_sent = set()
		self.init(*args, **kwargs)
		
	def init(self, address='tcp://127.0.0.1'):
		import dill
		import process
		self.address = address
		self.context = zmq.Context()
		self.socket  = self.context.socket(zmq.PAIR)
		self.port    = self.socket.bind_to_random_port(address)
		self.process = process.spawn(Worker, address, self.port)
		
	def cached_func(self, func):
		funcid = hash(func)
		if funcid not in self.funcs_sent:
			self.funcs_sent |= set([funcid])
			return funcid, func
		return funcid, None

	def __call__(self, func, *args, **kwargs):
		funcid, cached_func = self.cached_func(func)
		self.send(funcid, cached_func, args, kwargs)
		response = self.recv()
		if 'error' in response:
			# dang, the worker threw an exception when runnning the func
			raise RemoteException(self.process, response['error'])
		return response['ok']

	def send(self, *args):
		if self.socket:
			self.socket.send(dill.dumps(args))
		
	def recv(self):
		return dill.loads(self.socket.recv())

	def kill(self):
		self.send(None, None, None, None)

	@property
	def alive(self):
	    return self.process.is_alive()
	
	def __del__(self):
		self.kill()

	@property
	def pid(self):
		return self.process.pid

class RemoteException(Exception):
	
	def __init__(self, process, defined):
		self.exception = defined['exception']
		self.traceback = defined['traceback']
		self.process = process

	def __str__(self):
		strings = []
		string += ['-----Remote Process: %d (%s)----'%(self.process.pid, self.process.name)]
		string += ['%s'%(self.traceback)]
		string += ['-'*len(strings[0])]
		return '\n'.join(strings)

